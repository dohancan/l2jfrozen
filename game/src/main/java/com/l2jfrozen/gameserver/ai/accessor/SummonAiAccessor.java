package com.l2jfrozen.gameserver.ai.accessor;

import com.l2jfrozen.gameserver.ai.L2SummonAI;
import com.l2jfrozen.gameserver.model.L2Object;
import com.l2jfrozen.gameserver.model.L2Summon;
import com.l2jfrozen.gameserver.model.actor.stat.SummonStat;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 22:34
 */
public abstract class SummonAiAccessor extends AIAccessor<SummonStat, L2SummonAI,L2Summon<L2SummonAI,SummonStat>> {

    public boolean isAutoFollow() {
        return getActor().getFollowStatus();
    }

    public void doPickupItem(L2Object object) {
        getActor().doPickupItem(object);
    }
}

package com.l2jfrozen.configuration;

/**
 * User: vdidenko
 * Date: 11/23/13
 * Time: 3:07 PM
 */
public class DatabaseConfig implements ConfigManagerObserver {
    private static ConfigManager configManager = ConfigManager.getInstance();
    private String poolType;
    private String driver;
    private String url;
    private String login;
    private String password;
    private int maxConnections;
    private int maxIdleTime;
    private int connectionTimeout;
    private boolean debugMode = false;

    @Override
    public void configurationLoad() {

        poolType = configManager.getString("database.connection.pool.type");
        driver = configManager.getString("database.driver");
        url = configManager.getString("database.url");
        login = configManager.getString("database.login");
        password = configManager.getString("database.password");
        maxConnections = configManager.getInteger("database.connection.maximum");
        maxIdleTime = configManager.getInteger("database.connection.idle.time.ms");
        debugMode = configManager.getBoolean("database.debug.mode");
        connectionTimeout = configManager.getInteger("SingleConnectionTimeOutDb");
    }

    public String getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getMaxIdleTime() {
        return maxIdleTime;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

}

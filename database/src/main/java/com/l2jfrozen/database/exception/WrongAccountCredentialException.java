package com.l2jfrozen.database.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * vdidenko
 * 25.12.13
 */
public class WrongAccountCredentialException extends Exception {

    public WrongAccountCredentialException(String message) {
        super(message);
    }
}
